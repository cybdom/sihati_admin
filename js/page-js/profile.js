var doctoruid;
var doctorProfile;
firebase.auth().onAuthStateChanged(function(doctor) {
    if (doctor) {
        doctoruid = doctor.uid;
        getStarted(doctor);
    }
});
function getStarted( doctor ){
    firebase.database().ref('doctors/'+doctoruid+'/public').once('value').then(function(snapshot){
        doctorProfile = snapshot.val();
        $("#nom").attr('value',doctorProfile.nom);
        $("#prenom").attr('value',doctorProfile.prenom);
        $("#num_patient_max").text(doctorProfile.maxdayclients);
        $("#slider-range-max" ).slider({value:doctorProfile.maxdayclients});
        $("#bio").text(doctorProfile.biographie);
        setGenre(doctorProfile.sex);
        setSpecialite(doctorProfile.specialite);
        setWorkDays(doctorProfile.workdays);
        setLocationOnMap(doctorProfile.location);
    });
}
$("#submit").click(function(e){
    var location;
    location = marker.getPosition();
    location = location.lat() + ', ' + location.lng();
    e.preventDefault();
    var public = {
        nom : $("#nom").val(),
        prenom : $("#prenom").val(),
        sex : $("#genre").val(),
        biographie : $("#bio").val(),
        location : location,
        specialite : $("#sel1").val(),
        maxdayclients : $("#num_patient_max").text(),
        workdays : $('#work_day').val().toString()
    }
    firebase.database().ref('/doctors/'+doctoruid).update({
        public
    }, function(error){
        if( error ){
            $("#updateResponse").attr('class','text-danger');
            $("#updateResponse").text("Veuillez réessayer");
        }else{
            $("#updateResponse").attr('class','text-success');
            $("#updateResponse").text("Votre profile a été mis à jour !");
            window.scrollTo(0,0);
        }
    });
});
function setGenre(genre){
    $('#genre').selectpicker('val',genre);
}
function setWorkDays(days){
    days = days.trim();
    days = days.split(',');
    $('#work_day').selectpicker('val',days);
}
function setSpecialite(specialite){
    $("#sel1").selectpicker('val',specialite);
}
function setLocationOnMap(location){
    var index = location.indexOf(',');
    var lat = location.slice(0, index);
    lat = lat.trim();
    lat = Number(lat);
    var long = location.slice(index,location.length);
    long = long.replace(',','');
    long = long.trim();
    long = Number(long);
    location = {
        lat: lat,
        lng : long
    };
    map.setCenter(location);
    placeMarker(location);
}