firebase.auth().onAuthStateChanged(function(doctor) {
    if (doctor) {
        getDoctorData(doctor);
    }
});
function getDoctorData( doctor ){
    firebase.database().ref('doctors/'+doctor.uid+'/private').once('value').then(function(snapshot) {
            doctorData = snapshot.val();
            getPatientList(snapshot.val().patients.today.en_attente);
            const app = new Vue({
                el: "#app",
                data: {
                    doctorData
                }
            });
            setCharts();
        }
    );
}
function getPatientList(snapshot){
    $.each(snapshot, function(index, patient) {
        console.log(patient);
        if ( patient.status == "ongoing"){var status_string = "En cours"; var status_class ="text-primary"}
        else { var status_string = "En attente"; var status_class = "text-warning"; }
        $("#patients_list").append(
            '<tr id=patient"'+ patient.id + '><th scope="row">'+patient.id+'</th>'+
                '<td>'+patient.nom+
                '</td><td>'+patient.prenom+
                '</td><td>'+patient.date_naissance+
            '</tr>'
        );
    }); 
    
}