firebase.auth().onAuthStateChanged(function(doctor) {
    if (doctor) {
        getPatientList(doctor);
    }
});
function getPatientList( doctor ){
    var patientsList;
    firebase.database().ref('doctors/'+doctor.uid+'/private/archive').once('value').then(function(snapshot) {
        $.each(snapshot.val(), function(index, patient){
            patientsList += '<tr id=patient"'+ index + '><th scope="row">'+index+'</th>'+
                '<td>'+patient.nom+
                '</td><td>'+patient.prenom+
                '</td><td>'+patient.date_naissance+
                '</td><td>'+patient.date_visite+
                '</td><td>'+patient.diagnostique+
                '</td><td>'+patient.origin+
                '</td><td>'+patient.prix+
            '</tr>';
        });
        $("#patients_list").html(patientsList);
    });
}