var doctoruid;
var functions = firebase.functions();
firebase.auth().onAuthStateChanged(function(doctor) {
    if (doctor) {
        doctoruid = doctor.uid;
        getStarted(doctor);
    }
});
function getStarted( doctor ){
    // updatePatientList(  );
    var patient = {
        nom : $("#nom").val(),
        prenom : $("#prenom").val(),
        naissance : $("#dateNaissance").val(),
        genre : $("#sel1").find(":selected").text()
    };
    var addPatient = firebase.functions().httpsCallable('addMessage');
    addPatient({text: "test"}).then(function(result) {
        console.log(result)
    });
}
// function addPatientToList(){
//     firebase.database().ref('doctors/' + doctoruid + "/private/patients/today/").once('value').then(function(snapshot){
//         var patient = {
//             nom : $("#nom").val(),
//             prenom : $("#prenom").val(),
//             naissance : $("#dateNaissance").val(),
//             genre : $("#sel1").find(":selected").text(),
//             id : snapshot.val().todayPatients
//         };
//         if ( (patient.nom.length * patient.prenom.length * patient.naissance.length * patient.genre.length) != 0){
//             $("#ajout_patient_response").text('');
//             writeClient(doctoruid, patient.nom, patient.prenom, patient.naissance, patient.genre);
//         }
//         else{
//             $("#ajout_patient_response").text('Veuillez compléter toutes les entrées');
//         }
//     });    
// }
// function writeClient(doctorId, patientNom, patientPrenom, patientNaissance, patientGenre) {
//     firebase.database().ref('doctors/'+doctorId+'/public').once('value').then(function(snapshot){
//         var patientid = snapshot.val().current;
//         console.log(patientid);
//         firebase.database().ref('doctors/' + doctorId + "/private/patients/today/en_attente/patient"+patientid).set({
//             nom : patientNom, 
//             prenom : patientPrenom, 
//             date_naissance : patientNaissance,
//             genre : patientGenre,
//             id: patientid
//         }, function (error){
//          if (error){
//              console.log(error);
//              $("#ajout_patient_response").text(error.message);
//          }   else{
//              $("#ajout_patient").modal('toggle');
//          }
//         });
//     }); 
    
// }

// function passerClient(patientId){
//     firebase.database().ref('doctors/' + doctoruid + '/private/patients/today/en_attente/patient'+ patientId).update({"/status" : "ongoing"},
//     function(error){
//         if (error){
//             alert('Veuillez réessayer');
//         }else{
//             updatePatientList();
//         }
//     });
//     firebase.database().ref('doctors/' + doctoruid + '/public').update({"/current" : patientId},
//     function(error){
//         if (error){
//             alert('Veuillez réessayer');
//         }else{
//             updatePatientList();
//         }
//     });
// }
// function addFinaliserId(id){
//     $("#finaliserPatient").attr("onclick","finaliserPatient("+id+")");
// }
// function getDate(){
//     var today = new Date();
//     var dd = today.getDate();
//     var mm = today.getMonth() + 1; //January is 0!
//     var yyyy = today.getFullYear();

//     if (dd < 10) {
//     dd = '0' + dd;
//     }

//     if (mm < 10) {
//     mm = '0' + mm;
//     }

//     today = mm + '/' + dd + '/' + yyyy;
//     return today;
// }
// function finaliserPatient(id){
//     diagnostique = $("#maladies").val();
//     prix = $("#prix").val();
//     firebase.database().ref('doctors/'+doctoruid+'/private').once('value').then(function(snapshot){
//         archiveId = snapshot.val().totalPatients + 1;
//         firebase.database().ref('doctors/'+doctoruid+'/private/patients/today/en_attente/patient'+id).once('value').then(function(snapshot){
//             firebase.database().ref('doctors/' + doctoruid + "/private/archive/patient"+archiveId).set({
//                 nom : snapshot.val().nom,
//                 prenom : snapshot.val().prenom,
//                 date_naissance : snapshot.val().date_naissance,
//                 date_visite : getDate(),
//                 diagnostique : diagnostique,
//                 prix : prix
//             },function (error){
//                 if (error){
//                     alert("Veuillez réessayer");
//                 }   else{
//                     firebase.database().ref('doctors/'+doctoruid+'/private/patients/today/en_attente/patient'+id).remove();
//                     updatePatientList();
//                     $("#finalisation_patient").modal('toggle');
//                 }
//             });
//         });
//     });  
// }

// function updatePatientList(){
//     firebase.database().ref('doctors/'+doctoruid+'/private/patients/today/en_attente').on('value',function(snapshot){
//         var tableRow = "";
//         $.each(snapshot.val(), function(index, patient){
//             lastPatientId = patient.id;
//             if ( patient.status == "ongoing"){var status_string = "En cours"; var status_class ="text-primary"}
//             else { var status_string = "En attente"; var status_class = "text-warning"; }
//             tableRow += 
//             '<tr id=patient"'+ patient.id + '><th scope="row">'+patient.id+'</th>'+
//                 '<td>'+patient.nom+
//                 '</td><td>'+patient.prenom+
//                 '</td><td>'+patient.date_naissance+
//                 '</td><td>'+
//                     '<div class="btn-group">'+
//                         '<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
//                             'Action'+
//                         '</button>'+
//                         '<div class="dropdown-menu">'+
//                             '<a onclick="passerClient('+ patient.id + ')" class="dropdown-item" href="#">Passer</a>'+
//                             '<a onclick="addFinaliserId('+ patient.id + ')" class="dropdown-item" href="#" data-toggle="modal" data-target="#finalisation_patient">Finaliser</a>'+
//                             '<a onclick="removeRdv('+ patient.id + ')" class="dropdown-item" href="#">Supprimer</a>'+
//                         '</div>'+
//                     '</div>'+
//                 '</td>'+
//                 '<td class="'+status_class+'">'+status_string+'</td>'+
//             '</tr>';
//         });
//         $("#patients_list").html(tableRow);
//     });
// }
// function updateCurrent(){
    
// }
// function removeRdv(id){
//     var current = 0;
//     firebase.database().ref('doctor/'+doctoruid+'/public').once('value').then(function(snapshot){
//         current = snapshot.val().status;
//     });
//     firebase.database().ref('doctors/'+doctoruid+'/private/patients/today/en_attente/patient'+id).remove();
//     if ( id <= current ){
//         firebase.database().ref('doctors/' + doctoruid + '/public').update({"/current" : patientId},
//         function(error){
//             if (error){
//                 alert('Veuillez réessayer');
//             }else{
//                 updatePatientList();
//             }
//         });
//     }

// }