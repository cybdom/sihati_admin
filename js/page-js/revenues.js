firebase.auth().onAuthStateChanged(function(doctor) {
    if (doctor) {
        getDoctorData(doctor);
    }
});
function getDoctorData( doctor ){
    firebase.database().ref('doctors/'+doctor.uid+'/private').once('value').then(function(snapshot) {
            doctorData = snapshot.val();
            const app = new Vue({
                el: "#app",
                data: {
                    doctorData
                }
            });
        }
    );
}