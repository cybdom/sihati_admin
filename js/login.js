// Initializing Firebase

var config = {
    apiKey: "AIzaSyDk3-LVG7PgKfUgL49TmQRf9sMUW0rbPjw",
    authDomain: "sihati-29edf.firebaseapp.com",
    databaseURL: "https://sihati-29edf.firebaseio.com",
    projectId: "sihati-29edf",
    storageBucket: "sihati-29edf.appspot.com",
    messagingSenderId: "1047820654494"
};
firebase.initializeApp(config);


// Login function
$("#connect").click(function(e){
    e.preventDefault();
    var email = $("#email").val();
    var password = $("#password").val();
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
        .then(function() {
            return firebase.auth().signInWithEmailAndPassword(email, password);
    })
    .catch(function(error) {
        var errorCode = error.code;
        $("#status").removeClass("text-success");
        $("#status").addClass("text-danger");
        $("#status").text(returnErrorMessage(errorCode));
    });

});


// Reset Password function
$("#reset-password").click(function(e){
    e.preventDefault();
    var email = $("#email").val();
    firebase.auth().sendPasswordResetEmail(email).then(function() {
        $("#status").removeClass("text-danger");
        $("#status").addClass("text-success");
        $("#status").text("un email a été envoyé pour mettre à jour votre mot de passe");
    }).catch(function(error) {
        $("#status").removeClass("text-success");
        $("#status").addClass("text-danger");
        $("#status").text(returnErrorMessage(error["code"]));
    })
});

// Checking for user login state
firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        window.location.replace("/sihati_admin/index.php");
    }
  });
  
// Creating custom error messages
function returnErrorMessage(errorCode){
    var errorsArray = [
        "auth/invalid-email", "auth/wrong-password", "auth/user-not-found"
    ];
    if ( errorCode.localeCompare(errorsArray[0]) == 0 ){
        return "L'adresse email est mal formatée";
    }
    else if ( errorCode.localeCompare(errorsArray[1]) == 0 ){
        return "Le mot de passe n'est pas valide";
    }
    else if ( errorCode.localeCompare(errorsArray[2]) == 0 ){
        return "Il n'y a pas d'enregistrement d'utilisateur correspondant à cet identifiant.";
    }
}