var config = {
    apiKey: "AIzaSyDk3-LVG7PgKfUgL49TmQRf9sMUW0rbPjw",
    authDomain: "sihati-29edf.firebaseapp.com",
    databaseURL: "https://sihati-29edf.firebaseio.com",
    projectId: "sihati-29edf",
    storageBucket: "sihati-29edf.appspot.com",
    messagingSenderId: "1047820654494"
};
firebase.initializeApp(config);
var publicDoctorData;
firebase.auth().onAuthStateChanged(function(user) {
    if (!user) {
        window.location.replace("/sihati_admin/login.php");
    }else{
        firebase.database().ref('doctors/'+user.uid+'/public').once('value').then(function(snapshot) { 
            publicDoctorData = snapshot.val();       
            const app = new Vue({
                el: "#sidebar",
                data: {
                    publicDoctorData
                }
            });
        });
    }
});

$("#logout").click(function(){
    firebase.auth().signOut().then(function() {
        window.location.replace("/sihati_admin/login.php");
      }).catch(function(error) {
          console.log("error " + error);
      });
});