var color = Chart.helpers.color;
var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
var number_config = {
    type: 'line',
    data: {
        labels: ['Janvier', 'février', 'mars' ,'avril' ,'mai' ,'juin' ,'juillet'],
        datasets: [{
            label: 'Nombres de Patients',
            backgroundColor: '#f44336',
            borderColor: '#f44336',
            data: [
                123,823,572,728,123,823,572,728
            ],
            fill: false,
        }]
    },
    options: {
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Mois'
                }
            }],
        }
    }
};
var randomScalingFactor = function() {
    return Math.round(Math.random() * 100);
};
var origin_config = {
    type: 'doughnut',
    data: {
        datasets: [{
            data: [
                35,85
            ],
            backgroundColor: ['#2196F3','#f44336','#4CAF50'],
            label: 'Dataset 1'
        }],
        labels: [
            'En personne',
            'Application'
        ]
    },
    options: {
        responsive: true,
        legend: {
            position: 'top',
            labels: {
                defaultFontSize: '20'
            }
        },
        animation: {
            animateScale: true,
            animateRotate: true
        }
    }
};

var age_config = {
    type: 'bar',
    data: {
        labels: ['0 - 10 ans', '10 - 18 ans', '18 - 40 ans' ,'40 - 60 ans' ,'60+ ans'],
        datasets: [{
            label: "Patients",
            backgroundColor: '#2196F3',
            borderColor: '#2196F3',
            borderWidth: 1,
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ]
        }]
    },
    options: {
        responsive: true,
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: "Tranche D'age"
                }
            }],
        }
    }
};
var sex_config = {
    type: 'doughnut',
    data: {
        datasets: [{
            data: [
                randomScalingFactor(),randomScalingFactor()
            ],
            backgroundColor: ['#2196F3','#f44336'],
            label: 'Sexe des patients'
        }],
        labels: [
            'Hommes',
            'Femmes'
        ]
    },
    options: {
        responsive: true,
        legend: {
            position: 'top',
            labels: {
                defaultFontSize: '20'
            }
        },
        animation: {
            animateScale: true,
            animateRotate: true
        }
    }
};
function setCharts() {
    var number = document.getElementById('number').getContext('2d');
    var origin = document.getElementById('origin').getContext('2d');
    var age = document.getElementById('age').getContext('2d');
    var sex = document.getElementById('gender').getContext('2d');
    window.myLine = new Chart(number, number_config);
    window.myLine = new Chart(origin, origin_config);
    window.myBar  = new Chart(age, age_config);
    window.myLine = new Chart(sex, sex_config);
};
