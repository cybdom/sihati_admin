<?php include("header.php"); ?>
<body>
    <style>
        #map{
            height: 500px;
        }
    </style>
    <div class="wrapper" id="app">
        <img class="logo" src="imgs/logo.png" />
        <section class="topbar">
            <h5 class="topbar__header text-center">Tableau de bord</h5>
        </section>
        <?php include('sidebar.php'); ?>
        <section class="content container-fluid">
            <div class="row justify-content-md-center">
                <div class="col-8 profile" id="profile">
                    <div class="card card--shadow card--large profile--content">
                        <h5 id="updateResponse"></h5>
                        <div class="form-group">
                            <label for="nom">Nom</label>
                            <input name="nom" type="text" class="form-control" id="nom" placeholder="Votre Nom">
                        </div>
                        <div class="form-group">
                            <label for="prenom">Prenom</label>
                            <input name="prenom" type="text" class="form-control" id="prenom" placeholder="Votre Prenom">
                        </div>
                        <div class="form-group">
                            <label for="genre">Genre</label>
                            <select name="genre" class="form-control" id="genre" title="Choisir">
                                <option>Homme</option>
                                <option>Femme</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="work_day">Jours de travail</label>
                            <select id="work_day" name="work_day" class="form-control work_day" multiple data-live-search="true" title="Jours de travail">
                                <option>Dimanche</option>
                                <option>Lundi</option>
                                <option>Mardi</option>
                                <option>Mercredi</option>
                                <option>Jeudi</option>
                                <option>Vendredi</option>
                                <option>Samedi</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="nb_max_patient">Nombres de patients maximum par jour: <span id="num_patient_max"></span></label>
                            <div id="slider-range-max"></div>
                        </div>
                        <div class="form-group">
                            <label for="specialite">Specialite</label>
                            <select name="specialite" class="form-control" id="sel1" title="Choisir">
                                <option>médecine générale</option>
                                <option>allergologue - l'immunologue</option>
                                <option>anesthésiologue</option>
                                <option>andrologue</option>
                                <option>cardiologue</option>
                                <option>chirurgie</option>
                                <option>chirurgie cardiaque</option>
                                <option>chirurgie esthétique, plastique et reconstructive</option>
                                <option>chirurgie générale</option>
                                <option>chirurgie maxillo-faciale</option>
                                <option>chirurgie pédiatrique</option>
                                <option>chirurgie thoracique</option>
                                <option>chirurgie vasculaire</option>
                                <option>neurochirurgie</option>
                                <option>dermatologue</option>
                                <option>endocrinologue</option>
                                <option>gastro-entérologue</option>
                                <option>gériatrie</option>
                                <option>gynécologue</option>
                                <option>hématologue</option>
                                <option>hépatologue</option>
                                <option>infectiologue</option>
                                <option>médecine aiguë</option>
                                <option>médecine du travail</option>
                                <option>médecine interne</option>
                                <option>médecine palliative</option>
                                <option>médecine physique</option>
                                <option>médecine préventive</option>
                                <option>néonatologue</option>
                                <option>néphrologue</option>
                                <option>neurologue</option>
                                <option>odontologue</option>
                                <option>oncologue</option>
                                <option>obstétrique</option>
                                <option>ophtalmologue</option>
                                <option>orthopédie</option>
                                <option>Oto-rhino-laryngologue</option>
                                <option>pédiatrie</option>
                                <option>pneumologue</option>
                                <option>psychiatrie</option>
                                <option>radiologue</option>
                                <option>radiothérapie</option>
                                <option>rhumatologue</option>
                                <option>urologue</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="bio">Biographie</label>
                            <textarea name="bio" id="bio" rows="5" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="location">Localisation</label>
                            <div id="map"></div>
                            <input name="location" type="hidden" id="location">
                        </div>
                        <a href="#" id="submit" class="btn btn-outline-primary btn-lg">Enregistrer</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script>
        $( function() {
            $( "#slider-range-max" ).slider({
                range: "max",
                min: 1,
                max: 1000,
                value: 40,
                slide: function( event, ui ) {
                    $( "#num_patient_max" ).text( ui.value );
                }
                });
                $( "#num_patient_max" ).text( $( "#slider-range-max" ).slider( "value" ) );
        } );
        $( function() {
            var handle = $( "#custom-handle" );
            $( "#slider" ).slider({
            create: function() {
                handle.text( $( this ).slider( "value" ) );
            },
            slide: function( event, ui ) {
                handle.text( ui.value );
            }
            });
        } );

        var map;
        var marker;
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -34.397, lng: 150.644},
            zoom: 8
            });
            google.maps.event.addListener(map, 'click', function(event) {
                placeMarker(event.latLng);
            });
        }
        function placeMarker(location) {
            $("#location").text(location);
            if (marker == null)
            {
                marker = new google.maps.Marker({
                    position: location,
                    map: map
                }); 
            } 
            else {   marker.setPosition(location); } 
        }
        $('select').selectpicker();
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDk3-LVG7PgKfUgL49TmQRf9sMUW0rbPjw&callback=initMap"
        async defer></script>
    <script src="js/page-js/profile.js"></script>
</body>
</html>