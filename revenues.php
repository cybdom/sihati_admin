<?php include("header.php"); ?>
<body>
    <div class="wrapper" id="app">
        <img class="logo" src="imgs/logo.png" />
        <section class="topbar">
            <h5 class="topbar__header text-center">Revenues</h5>
        </section>
        <?php include('sidebar.php'); ?>
        <section class="content container-fluid">
        
        <div class="row mg bg--grey">
                <div class="col-12 col-md-3 col-sm-6  row--margin-onmedium">
                        <div class="card card--shadow bg--blue">
                            <div class="card-content">
                                <i class="far fa-user fa-5x"></i>
                                <p class="lead card__title">Nombres de patients de la journée</p>
                                <h4 class="card__content">{{doctorData.patients.today.todayPatients}}</h4>
                            </div>
                        </div>
                    </div>
                <div class="col-12 col-md-3 col-sm-6  row--margin-onmedium">
                    <div class="card card--shadow bg--blue">
                        <div class="card-content">
                            <i class="fas fa-list-ol fa-5x"></i>
                            <p class="lead card__title">Nombres Total de patients</p>
                            <h4 class="card__content">{{doctorData.totalPatients}}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-3 col-sm-6  row--margin-onmedium">
                    <div class="card card--shadow  bg--blue">
                        <div class="card-content">
                            <i class="far fa-money-bill-alt fa-5x"></i>
                            <p class="lead card__title">Revenues de la journée</p>
                            <h4 class="card__content">{{doctorData.patients.today.todayRevenue}} DZD</h4>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-3 col-sm-6  row--margin-onmedium">
                    <div class="card card--shadow  bg--blue">
                        <div class="card-content">
                            <i class="far fa-money-bill-alt fa-5x"></i>
                            <p class="lead card__title">Revenues Total</p>
                            <h4 class="card__content">{{doctorData.totalRevenue}} DZD</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-md-center" id="patientBooking">
                <div class="col">
                    
                    <div class="card card--shadow">
                        <div class="card-header">
                            <a href="#"><h3 class="card__title">Mes Revenues </h3></a>
                        </div>
                        <div class="card-body">   
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nom</th>
                                        <th scope="col">Prénom</th>
                                        <th scope="col">Prix</th>
                                        <th scope="col">Date du Rendez-vous</th>
                                        <th scope="col">Heures</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>1500DZD</td>
                                        <td>15/08/2019</td>
                                        <td>14:39</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>1500DZD</td>
                                        <td>15/08/2019</td>
                                        <td>14:39</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>1500DZD</td>
                                        <td>15/08/2019</td>
                                        <td>14:39</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">4</th>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>1500DZD</td>
                                        <td>15/08/2019</td>
                                        <td>14:39</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <nav aria-label="...">
                                <ul class="pagination justify-content-center">
                                  <li class="page-item">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                  </li>
                                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                                  <li class="page-item active" aria-current="page">
                                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                  </li>
                                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                                  <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                  </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>
    <script src="js/chart.js"></script>
    <script src="js/page-js/revenues.js"></script>
</body>
</html>