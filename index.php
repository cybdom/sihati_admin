<?php include("header.php"); ?>
<body>
    <div class="wrapper" id="app">
        <img class="logo" src="imgs/logo.png" />
        <a class="show-sidebar" href="#"><i class="fas fa-bars fa-2x"></i> </a>
        <section class="topbar" id="test">
            <h5 class="topbar__header text-center">Tableau de bord</h5>
        </section>
        <?php include('sidebar.php'); ?>
        <section class="content container-fluid">
            <div class="row mg bg--grey">
            <div class="col-12 col-md-3 col-sm-6  row--margin-onmedium">
                        <div class="card card--shadow bg--blue">
                            <div class="card-content">
                                <i class="far fa-user fa-5x"></i>
                                <p class="lead card__title">Nombres de patients de la journée</p>
                                <h4 class="card__content">{{doctorData.patients.today.todayPatients}}</h4>
                            </div>
                        </div>
                    </div>
                <div class="col-12 col-md-3 col-sm-6  row--margin-onmedium">
                    <div class="card card--shadow bg--blue">
                        <div class="card-content">
                            <i class="fas fa-list-ol fa-5x"></i>
                            <p class="lead card__title">Nombres Total de patients</p>
                            <h4 class="card__content">{{doctorData.totalPatients}}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-3 col-sm-6  row--margin-onmedium">
                    <div class="card card--shadow  bg--blue">
                        <div class="card-content">
                            <i class="far fa-money-bill-alt fa-5x"></i>
                            <p class="lead card__title">Revenues de la journée</p>
                            <h4 class="card__content">{{doctorData.patients.today.todayRevenue}} DZD</h4>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-3 col-sm-6  row--margin-onmedium">
                    <div class="card card--shadow  bg--blue">
                        <div class="card-content">
                            <i class="far fa-money-bill-alt fa-5x"></i>
                            <p class="lead card__title">Revenues Total</p>
                            <h4 class="card__content">{{doctorData.totalRevenue}} DZD</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-md-center">
                <div class="col">
                    <div class="card card--shadow">
                        <div class="card-header">
                            <h3 class="card__title">Statistiques</h3>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="chart chart--container">
                                    <h4>Nombres de patients:</h4>
                                    <canvas class="chart-container--canvas" id="number" width="800" height="400"></canvas>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="chart chart--container">
                                    <h4>Origine des patients: </h4>
                                    <canvas class="chart-container--canvas" id="origin" width="800" height="400"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="chart chart--container">
                                    <h4>Tranche d'âge des patients:</h4>
                                    <canvas class="chart-container--canvas" id="age" width="800" height="400"></canvas>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="chart chart--container">
                                    <h4>Sexe des patients: </h4>
                                    <canvas class="chart-container--canvas" id="gender" width="800" height="400"></canvas>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
            <div class="row justify-content-md-center" id="patientBooking">
                <div class="col">
                    
                    <div class="card card--shadow">
                        <div class="card-header clearfix">
                        <a href="mes_rdv.php"><h3 class="card__title float-left">Liste des rendez-vous </h3></a>
                            <a href="mes_rdv.php" class="btn btn-outline-primary float-right">
                                <i class="fas fa-plus"></i> Ajouter à la liste
                            </a>
                        </div>
                        <div class="card-body">   
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nom</th>
                                        <th scope="col">Prénom</th>
                                        <th scope="col">Date de naissance</th>
                                    </tr>
                                </thead>
                                <tbody id ="patients_list">
                                    <!-- <tr>
                                        <th scope="row">1</th>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>
    <script src="js/datepicker.js"></script>
    <script src="js/show-sidebar.js"></script>
    <script src="js/page-js/index.js"></script>
    <script src="js/chart.js"></script>
</body>
</html>