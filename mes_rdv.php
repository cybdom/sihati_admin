<?php include("header.php");?>
<body>
    <div id="app" class="wrapper">
        <img class="logo" src="imgs/logo.png" />
        <section class="topbar">
            <h5 class="topbar__header text-center">Mes Rendez-vous</h5>
        </section>
        <?php include('sidebar.php'); ?>
        <section class="content container-fluid">
            <div class="row justify-content-md-center" id="patientBooking">
                <div class="col">
                    
                    <div class="card card--shadow">
                        <div class="card-header clearfix">
                        <a href="#"><h3 class="card__title float-left">Liste des rendez-vous </h3></a>
                            <a href="#" class="btn btn-outline-primary float-right" data-toggle="modal" data-target="#ajout_patient">
                                <i class="fas fa-plus"></i> Ajouter à la liste
                            </a>
                        </div>
                        <div class="card-body">   
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nom</th>
                                        <th scope="col">Prénom</th>
                                        <th scope="col">Date de naissance</th>
                                        <th scope="col">Action</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody id="patients_list">
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <nav aria-label="...">
                                <ul class="pagination justify-content-center">
                                  <li class="page-item">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                  </li>
                                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                                  <li class="page-item active" aria-current="page">
                                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                  </li>
                                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                                  <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                  </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php include("modals.php"); ?>
    <div class="modal fade" id="finalisation_patient" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Finalisation du Patient</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="prix">Prix</label>
                            <input class="form-control" id="prix" placeholder="Prix">
                        </div>
                        <div class="form-group">
                            <div class="ui-widget">
                                <label for="maladies" class="active">Diagnostique</label>
                                <input type="search" id="maladies" class="form-control" placeholder="Diagnostique...">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Annuler</button>
                    <button id="finaliserPatient" type="button" class="btn btn-success">Finaliser</button>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="js/maladies.js"></script>
    <script src="js/datepicker.js"></script>
    <script src="js/page-js/mes_rdv.js"></script>
</body>
</html>