<?php include("header.php"); ?>
<body>
    <div class="wrapper" id="app">
        <img class="logo" src="imgs/logo.png" />
        <section class="topbar">
            <h5 class="topbar__header text-center">Mes Patients</h5>
        </section>
        <?php include('sidebar.php'); ?>
        <section class="content container-fluid">
            <div class="row justify-content-md-center" id="patientBooking">
                <div class="col">
                    
                    <div class="card card--shadow">
                        <div class="card-header clearfix">
                            <h3 class="card__title float-left">Liste des patients </h3>
                        </div>
                        <div class="card-body">   
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nom</th>
                                        <th scope="col">Prénom</th>
                                        <th scope="col">Date de naissance</th>
                                        <th scope="col">Date de visite</th>
                                        <th scope="col">Diagnostique</th>
                                        <th scope="col">Origin</th>
                                        <th scope="col">Prix</th>
                                    </tr>
                                </thead>
                                <tbody id="patients_list">
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <nav aria-label="...">
                                <ul class="pagination justify-content-center">
                                  <li class="page-item">
                                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                                  </li>
                                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                                  <li class="page-item active" aria-current="page">
                                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                                  </li>
                                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                                  <li class="page-item">
                                    <a class="page-link" href="#">Next</a>
                                  </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>
    <script src="js/maladies.js"></script>
    <script src="js/chart.js"></script>
    <script src="js/page-js/patients.js"></script>
</body>
</html>