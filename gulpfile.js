var gulp = require('gulp'),
postcss = require('gulp-postcss'),
autoprefixer = require('autoprefixer'),
cssvars = require('postcss-simple-vars'),
nested = require('postcss-nested'),
cssimport = require('postcss-import'),
mixins = require('postcss-mixins'),
hexrgba = require('postcss-hexrgba');
var minifyCss = require('gulp-minify-css');

gulp.task('minify-css', function() {
  return gulp.src('css/*.css')
    .pipe(postcss([cssimport,mixins,cssvars,nested,hexrgba,autoprefixer,minifyCss]))
    .pipe(gulp.dest('dist'));
});

gulp.task('watch:css', function () {
    gulp.watch('css/**/*.css', ['minify-css']);
});

gulp.task('default', ['minify-css']);