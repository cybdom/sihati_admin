<div class="modal fade" id="ajout_patient" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Nouveau Patient</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p class="lead text-danger text-center" id="ajout_patient_response"></p>
            <form>
                <div class="form-group">
                    <label for="nom">Nom</label>
                    <input type="text" class="form-control" id="nom" aria-describedby="nomHelp" placeholder="Nom du Patient">
                </div>
                <div class="form-group">
                    <label for="prenom">Prenom</label>
                    <input type="text" class="form-control" id="prenom" aria-describedby="prenomHelp" placeholder="Prenom du Patient">
                </div>
                <div class="form-group">
                    <label for="specialite">Genre</label>
                    <select name="specialite" class="form-control" id="sel1" title="Choisir">
                        <option>Homme</option>
                        <option>Femme</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="dateNaissance">Date de naissance</label>
                    <input id="dateNaissance" class="form-control datepicker" placeholder="Date de naissance du Patient"/>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            <button onclick="addPatientToList()" id="addPatientToList" type="button" class="btn btn-primary">Ajouter à la liste</button>
        </div>
        </div>
    </div>
</div>