<section class="sidebar">
    <div class="sidebar__list" id="sidebar">
        <ul class="list-group list-group-flush list-unstyled" style="width: 100%;">
            <li class="no-padding">
                <div class="sidebar__user">
                    <a href="profile.php"><img class="user__img" src="svg/doctor.svg"/></a>
                    <h5 class="user__name">Docteur: {{publicDoctorData.nom}} {{publicDoctorData.prenom}}</h5>
                    <a href="profile.php">Mon Profile</a>
                </div>
            </li>
            <li 
                <?php if (basename($_SERVER['PHP_SELF']) == "index.php"){ echo 'class="active"';} ?>>
                <a href="index.php">Tableau de bord</a>
            </li>
            <li 
                <?php if (basename($_SERVER['PHP_SELF']) == "mes_rdv.php"){ echo 'class="active"';} ?>>
                <a href="mes_rdv.php">Mes Rendez-vous</a>
            </li>
            <li 
                <?php if (basename($_SERVER['PHP_SELF']) == "patients.php"){ echo 'class="active"';} ?>>
                <a href="patients.php">Mes Patients</a>
            </li>
            <!-- <li 
                <?php if (basename($_SERVER['PHP_SELF']) == "revenues.php"){ echo 'class="active"';} ?>>
                <a href="revenues.php">Mes Revenues</a>
            </li> -->
            <li><a href="#" id="logout">Se Deconnecter</a></li>
        </ul>
    </div>
</section>